import React from 'react';
import type {Node} from 'react';
import {
  View,
  Linking,
  Alert,
} from 'react-native';
import { RNCamera } from 'react-native-camera';


const App: () => Node = () => {

  barcodeRecognized = (e) => {
    Linking.openURL(e.data);
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          width: '100%',
        }}
        onBarCodeRead={this.barcodeRecognized.bind(this)}
        type={RNCamera.Constants.Type.back}
      >
      </RNCamera>
    </View>
  );
};

export default App;
